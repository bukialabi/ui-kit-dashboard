# Generic UI Kit Dashboard
This project is exactly what the title suggests - a generic dashboard created using the [UI Kit framework](http://getuikit.com/).

<br/><br/>
## Getting Started
Follow the instructions on the [sass website](http://sass-lang.com) to install Sass.<br/>

Clone the project from its git repository:
```
git clone https://gitlab.com/bukialabi/ui-kit-dashboard
```
<br/>
Once cloned, install all the development dependencies on your computer by typing the following in your terminal:
```
npm install
```
<br/>
See deployment notes below on how to deploy the project on a live system.

<br/><br/>
### Prerequisites

This project uses npm as a build tool, so you will need to have nodejs installed in order to take advantage of the various scripts built into the package. Learn more by visiting the [nodejs website](https://nodejs.org/en/).<br/>

For information on how to install node and update npm, visit the [npm website](https://docs.npmjs.com/getting-started/installing-node).<br/>

<br/><br/>
### Developing & Customizing the Dashboard
To get started customizing the dashboard, open up your terminal and type:
```
npm start
```
This will:
* Open the dashboard in your default browser
* Start a development server that watches the folder for changes and automatically reloads the browser as you code
* Watch all sass files created in the sass folder and auto-generate their css equivalents in the css folder

<br/>
Documentation for the components used in this dashboard can be found at [the UI Kit's official site](https://getuikit.com/docs)

Happy coding!

<br/><br/>
## NPM Scrips
```
npm run clean
```
This will:
* Remove extra files and folders auto-generated during development.

<br/><br/>
## Running the tests
(Nothing yet)

<br/><br/>
### Break down into end to end tests
(nothing yet)

<br/><br/>
### And coding style tests
(nothing yet)

<br/><br/>
## Deployment
Every folder except the following should be placed on the http server of you choice:
* The Node Modules folder
* The Sass Folder
* The package.json file
* The readme file
* The license file

<br/><br/>
## Built With
* [UI Kit](http://getuikit.com/) - The UI framework used
* [Sass](http://sass-lang.com) - CSS Preprocessor

<br/><br/>
## Contributing
(Nothing Yet)

<br/><br/>
## Versioning
I use the most recent update date for versioning (format - YYYY.MM.DD).

<br/><br/>
## Authors
* **Adebanke Buki Alabi** - *Initial work* - [Generic Material Dashboard](gitlab.com/bukialabi/material-dashboard)

<br/><br/>
## License
This project is licensed under the ISC License - see the [LICENSE.md](LICENSE.md) file for details

<br/><br/><br/>
## Acknowledgments
(Nothing Yet)
